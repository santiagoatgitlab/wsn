import { Injectable } from '@angular/core';
import { LoggedUser } from './logged-user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiResponse } from './api-response';

@Injectable()
export class ApplicationService {
	
	getLoggedUserUrl : string = '/api/login/getUser';
	logOutUrl : string = '/api/login/logout';
	
	displayLogin 	: boolean = false;
	displayUserMenu : boolean = false;

	loggedUser : LoggedUser = {
		isLogged : false,
		userData: {}
	};

	constructor(private http: HttpClient) { }

	retrieveLoggedUser(){
		this.http.get<LoggedUser>(this.getLoggedUserUrl)
			.subscribe(
				response => {
					this.loggedUser = response;
				},
				error => {	
					console.log("there was a server error trying to retrieve logged user info");
				}	
			);
	}

	getLoggedUserData() : LoggedUser{
		return this.loggedUser.userData;
	}

	toggleLogin() : boolean {
		this.displayLogin = !this.displayLogin;
		return this.displayLogin;
	}

	toggleUserMenu() {
		this.displayUserMenu = this.displayUserMenu;
	}

	hidePopups(){
		this.hideLogin();
		this.hideUserMenu();
	}

	hideLogin(){
		this.displayLogin = false;
	}

	hideUserMenu(){
		this.displayUserMenu = false;
	}

	logOut(){
		this.http.get<ApiResponse>(this.logOutUrl)
			.subscribe(
				response => {
					if (response.status == 'ok'){
						this.loggedUser.isLogged = false;	
						this.loggedUser.userData = {};
					}
					else{
						console.log('Server error trying to logOut user');
					}
				},
				error => {
					console.log('Server connection error');
				}
			);
	}
}
