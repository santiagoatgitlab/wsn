// Get dependencies

const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const parseurl = require('parseurl');
const session = require('express-session');


// Get our API routes

const api = require('./server/routes/api');

const app = express();


app.use(session({
	secret : 'thewsnproject',
	saveUninitialized: false,
	resave: false
}));


// Parsers for post data

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: 'false'}));


// point static path to dist

app.use(express.static(path.join(__dirname, 'dist')));

// the session middleware

app.use(function(req,res,next){
	if (!req.session.views){
		req.session.views = {};
	}

	var pathname = parseurl(req).pathname;

	req.session.views[pathname] = (req.session.views[pathname] || 0) + 1;
	
	next();
});

app.get('/foo',function(req,res,next){
	res.send('the number of views is: ' + req.session.views['/foo']);
});

app.get('/bar',function(req,res,next){
	res.send('the number of views is: ' + req.session.views['/bar']);
});

// set our api routes

app.use('/api',api);


// Catch all other routes and return the index file
app.get('/login/newuser', (req,res) => {
	
	var response = {
		status : 'ok',
		errorMsg: '',
		content : 'this api works just fine'
	}

	res.send(response);
});

app.get('*',(req,res) => {
	res.sendFile(path.join(__dirname,'dist/index.html'));
});


/**
 * Get por from environment and store in express
 */

const port = process.env.PORT || '3000';
app.set('port',port);


/**
 * Create Http server
 */

const server = http.createServer(app);


/**
 * Listen on provided port, on all network interfaces
 */

server.listen(port, () => console.log(`API running on localhost:${port}`));
