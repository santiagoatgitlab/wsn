import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { ApplicationService } from '../application.service';

@Component({
	selector: 'app-user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {

	constructor(
		private loginService: LoginService,
		private applicationService: ApplicationService,
	) { }

	loggedUser: any;
	

	ngOnInit() {

		this.loggedUser = this.loginService.getLoggedUserData();

	}

}
