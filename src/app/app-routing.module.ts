import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';

const routes: Routes = [
	{ path: 'signup', component: SignupComponent },
	{ path: 'account/activate/:hash', component: ActivateAccountComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})

export class AppRoutingModule { }
