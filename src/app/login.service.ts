import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiResponse } from './api-response';

@Injectable()
export class LoginService {

	newUserUrl : string = '/api/login/newuser';
	activateAccountUrl : string = '/api/account/activate/';
	logInUrl : string = '/api/login/log';
	getLoggedUserUrl : string = '/api/login/getUser';
	logOutUrl : string = '/api/login/logout';

	userLogged : boolean = false;
	userData : any;

  	constructor(private http: HttpClient) { }

	newUser(userData: any) : Observable<ApiResponse>{
		return this.http.post<ApiResponse>(this.newUserUrl, userData);
	}

	activateAccount(hash: string) : Observable<ApiResponse>{
		return this.http.get<ApiResponse>(this.activateAccountUrl + hash);
	}

	logIn(userData: any) : Observable<ApiResponse>{
		let result = this.http.post<ApiResponse>(this.logInUrl, userData);
		result.subscribe(
			response => {
				if (response.status == "ok"){
					this.retrieveLoggedUser();
				}
				console.log(response.details);
			},
			error => {
				console.log('No response from server');
			}	
		);
		return result;
	}

	retrieveLoggedUser(){
		this.http.get<ApiResponse>(this.getLoggedUserUrl)
			.subscribe(
				response => {
					if (response.status == "ok"){
						if (response.content.userLogged){
							this.userLogged = true;		
							this.userData = response.content.userData;	
						}
					}
					console.log(response.details);
				},
				error => {	
					console.log('No response from server');
				}	
			);
	}

	getLoggedUserData() : any {
		return this.userData;
	}

	logOut(){
		this.http.get<ApiResponse>(this.logOutUrl)
			.subscribe(
				response => {
					if (response.status == 'ok'){
						this.userLogged = false;		
						this.userData = {};	
					}
					console.log(response.details);
				},
				error => {
					console.log('No response from server');
				}
			);
	}


}

