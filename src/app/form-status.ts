export enum FormStatus{
	notSent,
	sending,
	success,
	error
}
