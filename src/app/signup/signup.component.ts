import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../login.service';
import { Observable } from 'rxjs/Observable';
import { ApiResponse } from '../api-response';
import { FormStatus } from '../form-status';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

	currentStatus : FormStatus;
	formStatus = FormStatus;

	first_name : string;
	last_name : string;
	email : string;
	password : string;
	password_confirm : string;
	sending : boolean = false;

	constructor(private loginService: LoginService) { }

	ngOnInit() {
		this.currentStatus = FormStatus.notSent;
	}

	emailCheck(){
	}

	onSubmit(){
		this.newUser();	
	}

	newUser(){

	    let	data = {
			'firstName' : this.first_name,
			'lastName' : this.last_name,
			'email' : this.email,
			'password' : this.password,
			'confirmPassword' : this.password_confirm
		}
	
		this.currentStatus = FormStatus.sending;
		this.loginService.newUser(data)
			.subscribe( 
				response => {
					if (response.status == 'ok'){
						this.userCreated();
						this.currentStatus = FormStatus.success;	
					}
					else if (response.status == 'error'){
						this.signupError();
						this.currentStatus = FormStatus.error;
					}
				},
				err => {
					console.log('something went wrong');
					this.currentStatus = FormStatus.error;
				}
			);

	}

	userCreated(){
		console.log('User created succesfully');
	}

	signupError(){
		console.log('Error attempting to create user');
	}

}
