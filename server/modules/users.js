const mongodb 			= require('mongodb');
const mongoClient 		= mongodb.MongoClient;
const ObjectId 			= mongodb.ObjectID;
const mongoServerUrl 	= 'mongodb://localhost:27017/wsn';
const crypto 			= require('crypto');
const nodemailer 		= require('nodemailer');
const randomstring 		= require('randomstring');

var data;
var exitCallback;

activationStatus = {
	EMAIL_NOT_SENT : 0,
	EMAIL_SENT : 1,
	ACTIVATED : 2
}

exports.signUp = function(_data,_exitCallback){

	data 			= _data;
	exitCallback 	= _exitCallback;

	validateSignup(function(){
		generateHash();
	});

}

function validateSignup(isValid){
	var valid 	= true;
	var details = [];
	emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (!data.hasOwnProperty('firstName')){
		valid = false;			
		details.push('field first name cannot be empty');
	}

	if (!data.hasOwnProperty('lastName')){
		valid = false;			
		details.push('field last name cannot be empty');
	}

	if (data.hasOwnProperty('email')){
		if (data.email.length >= 6){
			if (!data.email.match(emailRegex)){
				valid = false;			
				details.push('field email not valid as an email address');
			}
		}
		else{
			valid = false;			
			details.push('field email cannot be less than 6 characters long');
		}
	}
	else{
		valid = false;			
		details.push('field email cannot be empty');
	}

	if (data.hasOwnProperty('password') && data.hasOwnProperty('confirmPassword')){
		if (data.password.length >= 6){
			if (!data.hasOwnProperty('confirmPassword') || data.password != data.confirmPassword){
				valid = false;			
				details.push("password fields don't match");
			}
		}
		else{
			valid = false;			
			details.push('field password cannot be less than 6 characters long');
		}
	}
	else{
		valid = false;			
		if (!data.hasOwnProperty('password')){
			details.push('field password cannot be empty');
		}
		else{
			details.push('field confirmPassword cannot be empty');
		}
	}

	delete data.confirmPassword;
	mongoClient.connect(mongoServerUrl,function(err,database){
		if (err) throw err;
		var wsnDb = database.db('wsn');
		wsnDb.collection('users').find({email:data.email}).toArray(function(err,result){
			if (err) throw err;
			if (result.length > 0){
				valid = false;
				details.push('value for field email already existant');
			}
			if (valid){
				isValid();
			}
			else{
				exitCallback({status: 'error',details:details});
			}
			database.close();
		});
	});

}


function generateHash(){
	
	mongoClient.connect(mongoServerUrl,function(err,database){
		var activationHash = randomstring.generate(24);
		database.db('wsn').collection('users').find({activationHash:activationHash}).toArray(function(err,result){
			if (err) throw err;
			if (result.length == 0){
				data.activationHash = activationHash;
				saveData();
			}
			else{
				generateHash();
			}
		});
	});

}

function saveData(){
	mongoClient.connect(mongoServerUrl,function(err,database){
		if (err) throw err;
		data.password = crypto.createHash('sha256').update(data.password).digest('base64');
		data.activationStatus = activationStatus.EMAIL_NOT_SENT;
		database.db('wsn').collection('users').insertOne(data, function(err,res){
			if (err) throw err;
			database.close();
			sendMail(res.insertedId);
			console.log(`Inserted id: ${res.insertedId}`);
			exitCallback({status : 'ok',details : `new user ${data.email} data inserted succesfully!`});
		});
	});
}

function sendMail(insertedId){
	
	let transporter = nodemailer.createTransport({
		service : 'gmail',
		auth: {
			user : 'gonzalezrojosantiago@gmail.com',
			pass : 'ajdreobnairjm6tbr4d'
		}		
	});

	let mailOptions = {
		from : 'gonzalezrojosantiago@gmail.com',
		to : data.email,
		subject : 'Wsn: Activación de cuenta',
		html : `<h3>Activación de cuenta</h3>Haga click en el siguiente link para activar su cuenta<br><a href="http://localhost:4200/account/activate/${data.activationHash}">activar cuenta</a>`
	}

	transporter.sendMail(mailOptions, function(error, info){
		if (error){
			console.log(error);
		}
		else{
			console.log('Email sent to ' + data.email + ' : ' + info.response);
			mongoClient.connect(mongoServerUrl,function(err,database){
				if (err) throw err;
				database.db('wsn').collection('users').update({_id:insertedId},{$set:{activationStatus:activationStatus.EMAIL_SENT}},function(err,result){
					if (err) throw err;
					database.close();
				});
			})
		}
	});

}

exports.activateAccount = function (activationHash,_exitCallback){
	
	mongoClient.connect(mongoServerUrl,function(err,database){
		if (err) throw err;
		database.db('wsn').collection('users')
			.update({activationHash:activationHash},{$set:{activationStatus:activationStatus.ACTIVATED},$unset:{activationHash:""}},function(err,result){
			if (err) throw err;
			database.close();
			if (result.result.nModified == 1){
				_exitCallback({status : 'ok',details : 'user activated'});
			}
			else{
				_exitCallback({status : 'ok',details : "couldn't activate user, probably already activated"});
			}
		});
	});
	
}

exports.logIn = function (data,_exitCallback){
	
	mongoClient.connect(mongoServerUrl,function(err,database){
		if (err) throw err;
		var encPassword = data.password = crypto.createHash('sha256').update(data.password).digest('base64');
		database.db('wsn').collection('users').findOne({
			email : data.email,
			password : encPassword,
			activationStatus : activationStatus.ACTIVATED
		},function(err,result){
			if (err) throw err;
			database.close();
			if (result == null){
				_exitCallback({status:"error",details:"email or password incorrect"},result);
			}
			else{
				_exitCallback({status:"ok",details:""},result);
			}
		});
	});
}
