import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { LoginService } from '../login.service';
import { Observable } from 'rxjs/Observable';
import { ApiResponse } from '../api-response';

@Component({
	selector: 'app-activate-account',
	templateUrl: './activate-account.component.html',
	styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {

	hash : string;
	message : string = 'Activating your account, please wait.';

	constructor(
		private loginService: LoginService,
		private route: ActivatedRoute,
		private location: Location
	) { }

	ngOnInit() {
			
		this.hash = this.route.snapshot.paramMap.get('hash');
		this.loginService.activateAccount(this.hash)
			.subscribe(		
				response => {
					this.message = response.details;
				},
				err => {
					console.log('Something went wrong on the server');
				}
			);
	}
	

}
