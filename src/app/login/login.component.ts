import { Component, OnInit, Input } from '@angular/core';
import { ApplicationService } from '../application.service';
import { LoginService } from '../login.service';
import { FormStatus } from '../form-status';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	currentStatus : FormStatus;
	formStatus = FormStatus;

	display : boolean = false;
	@Input() email: string = '';
	@Input() password: string = '';
	@Input() keepData: boolean = false;

	constructor(
		private applicationService: ApplicationService,
		private loginService: LoginService
	) { }

	ngOnInit(){
		this.currentStatus = FormStatus.notSent;
	}

	toggleDisplay(){
		this.display = this.applicationService.toggleLogin();
		console.log('now display var is: '+this.display);
	}

	sendLogin(){
		this.loginService.logIn({
			email : this.email,
			password : this.password,
			keepData : this.keepData
		}).subscribe( 
			response => {
				if (response.status == 'ok'){
					this.currentStatus = FormStatus.success;
				}
				else if (response.status == 'error'){
					this.currentStatus = FormStatus.error;
				}
			},
			err => {
				this.currentStatus = FormStatus.error;
			}
		);
	}

}

