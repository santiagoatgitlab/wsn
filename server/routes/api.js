const express = require('express');
const router = express.Router();
const users = require('../modules/users.js');


// Get api listing

router.get('/',(req,res) => {
	res.send('App works');
});

router.post('/login/newuser', (req,res) => {
	
	users.signUp(req.body,function(response){
		res.send(response);
	});
	

});

router.get('/account/activate/:hash', (req,res) => {
	
	console.log('trying to activate account with hash ' + req.params.hash);
	users.activateAccount(req.params.hash, function(response){
		res.send(response);
	});

});

router.post('/login/log',(req,res)=>{
	users.logIn(req.body,function(response,userData){
		if (response.status == "ok"){
			req.session.user = {
				userId 		: userData._id,					
				username 	: userData.email,					
				name 		: userData.firstName,					
				lastName 	: userData.lastName					
			};
			response.details = "user data in session";
		}
		res.send(response);
	});
});

router.get('/login/getUser',(req,res)=>{
	if (req.session.user){
		res.send({
			status: "ok",
			details: "user data retrieved",
			content:{
				userLogged : true,
				userData : req.session.user	
			} 
		});
	}
	else{
		res.send({
			status:"ok",
			details:"no user logged",
			content: { userLogged : false }
		});
	}
});

router.get('/login/logout',(req,res)=>{
	if (req.session.user){
		delete req.session.user;
		res.send({status:"ok",details:"user logged out"});
	}
	else{
		res.send({status:"ok",details:"user was already logged out"});
	}
});

module.exports = router;
