export interface ApiResponse {

	status: string;
	details: string;
	content: any;

}
