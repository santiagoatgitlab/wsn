import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { SignupComponent } from './signup/signup.component';
import { LoginService } from './login.service';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { ApplicationService } from './application.service';


@NgModule({
	declarations: [
		AppComponent,
		TopBarComponent,
		LoginComponent,
		SignupComponent,
		ActivateAccountComponent,
		UserMenuComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		HttpClientModule
	],
	bootstrap: [AppComponent],
	providers: [LoginService, ApplicationService]
})
export class AppModule { }
